
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke() {
  if(document.getElementById("stevilka").value == 1){
      document.getElementById("ime").value = "Športakus";
      document.getElementById("priimek").value = "Lazytown";
      document.getElementById("krvniTlak1").value = "115";
      document.getElementById("krvniTlak2").value = "75";
      document.getElementById("visina").value = "190";
      document.getElementById("tezina").value = "80";
  }
  if(document.getElementById("stevilka").value == 2){
      document.getElementById("ime").value = "Gandalf";
      document.getElementById("priimek").value = "The Grey";
      document.getElementById("krvniTlak1").value = "180";
      document.getElementById("krvniTlak2").value = "100";
      document.getElementById("visina").value = "185";
      document.getElementById("tezina").value = "70";
  }
  if(document.getElementById("stevilka").value == 3){
      document.getElementById("ime").value = "I am";
      document.getElementById("priimek").value = "Really fat";
      document.getElementById("krvniTlak1").value = "150";
      document.getElementById("krvniTlak2").value = "100";
      document.getElementById("visina").value = "200";
      document.getElementById("tezina").value = "150";
  }
 
}
function createEHR(){
    sessionId = getSessionId();;
    
    var ime = document.getElementById("ime").value;
    var priimek = document.getElementById("priimek").value;
    
    $.ajaxSetup({
        headers: {"Ehr-session": sessionId}
    });
    
    $.ajax({
        url:baseUrl + "/ehr",
        type : 'POST',
        success: function (data) {
                var ehrId = data.ehrId;
                var partyData = {
                    firstNames: ime,
                    lastNames: priimek,
                    partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
                };
                 $.ajax({
                    url: baseUrl + "/demographics/party",
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(partyData),
                    success: function (party) {
                        if (party.action == 'CREATE') {
                            $("#dodajEHR").val(ehrId);
                            
                        }
                        $("#izbira").append("<option value="+ehrId+">"+ime+" "+priimek+"</option>");
                    },
                 })
        }
    })
}
var tezina;
    var visina;
    var krvniTlak1inf;
    var krvniTlak2inf;
    var helpEhrId;

function dodajMeritve() {
   
     sessionId = getSessionId();
 
    var ehrId = document.getElementById("dodajEHR").value;
    var telesnaVisina = document.getElementById("visina").value;
    var telesnaTeza =document.getElementById("tezina").value;
    var krvniTlak1 = document.getElementById("krvniTlak1").value;
    var krvniTlak2 = document.getElementById("krvniTlak2").value;
 
    
        $.ajaxSetup({
            headers: {"Ehr-Session": sessionId}
        });
        var podatki = {
            // Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
            "ctx/language": "en",
            "ctx/territory": "SI",
            "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
            "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
            "vital_signs/blood_pressure/any_event/systolic": krvniTlak1,
            "vital_signs/blood_pressure/any_event/diastolic": krvniTlak2
            };
       
        var parametriZahteve = {
            ehrId: ehrId,
            templateId: 'Vital Signs',
            format: 'FLAT',
        };
        $.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(podatki),
        });
    
}
var visinaa = $("#visina").val();

 narisiGraf = function () {
 if(document.getElementById("stevilka").value == 1){
     var broj = 10;
 }
  if(document.getElementById("stevilka").value == 2){
      broj = 70;
  }
   if(document.getElementById("stevilka").value == 3){
       broj = 500;
   }
//Better to construct options first and then pass it as a parameter
	var options = {
		title: {
			text: "Column Chart using jQuery Plugin"
		},
                animationEnabled: true,
		data: [
		{
			type: "column", //change it to line, area, bar, pie, etc
			dataPoints: [
			     
				{ x: 50, y: broj }
			    
			]
		}
		]
	};

	$("#chartContainer").CanvasJSChart(options);

}

function prikaziMeritve() {
    
 
   
    var ehrId = document.getElementById("dodajEHR").value;
    
    
    $("#imed").html("<b>ehrId izbrane osebe: "+ehrId+"<br><br>");
    helpEhrId = ehrId;
 
    var searchData = [{key: "ehrId", value: ehrId}];
$.ajax({
    url: baseUrl + "/demographics/party/query",
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(searchData),
    success: function (res) {
       
        for (var i in res.parties) {
            var party = res.parties[i];
            $("#priimekd").html("<b>Izbrali ste osebo: "+party.firstNames + ' ' + party.lastNames + "<b><br>");
        }
    }
});

   
    var sessionId = getSessionId();
 
   
        $.ajax({
    url: baseUrl + "/view/" + helpEhrId + "/weight",
    type: 'GET',
    headers: {"Ehr-Session": sessionId},
    success: function (res) {
            $("#tezinainf").html("Teža: "+res[0].weight+" kg");
            teza =res[0].weight;
    }
});    
 
    $.ajax({
    url: baseUrl + "/view/" + helpEhrId + "/height",
    type: 'GET',
    headers: {"Ehr-Session": sessionId},
    success: function (res) {
       
        $("#visinainf").html("Visina: "+res[0].height+" cm");
            visina = res[0].height;
           }
});

 $.ajax({
    url: baseUrl + "/view/" + helpEhrId + "/weight",
    type: 'GET',
    headers: {"Ehr-Session": sessionId},
    success: function (res) {
            $("#tezinainf").html("Teža: "+res[0].weight+" kg");
            teza =res[0].weight;
    }
});  

 $.ajax({
    url: baseUrl + "/view/" + helpEhrId + "/blood_pressure",
    type: 'GET',
    headers: {"Ehr-Session": sessionId},
    success: function (res) {
            $("#krvniTlak1inf").html("Sistolični tlak: "+res[0].systolic);
            krvniTlak1inf =res[0].sysbp;
    }
});  
 
$.ajax({
    url: baseUrl + "/view/" + helpEhrId + "/blood_pressure",
    type: 'GET',
    headers: {"Ehr-Session": sessionId},
    success: function (res) {
            $("#krvniTlak2inf").html("Diastolični tlak: "+res[0].diastolic);
            krvniTlak2inf =res[0].diabp;
    }
});   
}



function izracunaj(){
    if($("#krvniTlak1").val() < 120 && $("#krvniTlak2").val() < 90){
         $("#izracunaj1").html("Stepen vgroženosti je 0!");
    }
    if($("#krvniTlak1").val() < 120 && $("#krvniTlak2").val() > 90){
         $("#izracunaj1").html("1!Poskrbite za diastolični tlak.");
    }
    if($("#krvniTlak1").val() > 170 && $("#krvniTlak2").val() < 90){
         $("#izracunaj1").html("2! Poskrbite za sistolični tlak!");
    }
    if($("#krvniTlak1").val() > 120 && $("#krvniTlak2").val() > 95){
         $("#izracunaj1").html("Stepen vgroženosti je 3! Poiščite pomoč.");
    }
    if($("#tezina").val() > 120){
         $("#izracunaj2").html("Stepen vgroženosti je McDonalds!");
    }
}
















// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
